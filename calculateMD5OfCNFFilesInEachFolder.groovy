/*
 * Copyright (c) 2017, Venkatesh-Prasad Ranganath
 *
 * BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath (rvprasad)
 */

import groovyx.gpars.GParsPool
import java.security.DigestInputStream
import java.security.MessageDigest

def generateMD5(final File file) {
    def md5 = MessageDigest.getInstance("MD5")
    file.withInputStream { is -> 
        def dis = new DigestInputStream(is, md5)
        while (dis.available()) {
            def len = Math.min(8192, dis.available())
            def buf = new byte[len]
            dis.read(buf, 0, len)
        }
    }
    md5.digest().encodeHex().toString()
}

def data = new File(".")
def dirNames = []
data.eachDirMatch(~/[A-Za-z0-9_-]+/, { dir ->
    def tmp2 = [dir]
    dir.traverse(type: groovy.io.FileType.DIRECTORIES) { f ->
        tmp2 << f.canonicalPath.drop(data.canonicalPath.length() + 1)
    }
    dirNames << tmp2
})

GParsPool.withPool() {
    dirNames.flatten().eachParallel { dirName ->
        def expDirName = data.canonicalPath + "/" + dirName
        def dir = new File(expDirName)
        def fileNames = []
        dir.eachFileMatch(~/.*\.cnf.*/, { file -> fileNames << file.name })
        if (fileNames.size()) {
            def tmp2 = new File(expDirName + "/checksum-md5.txt")
            if (tmp2.delete())
                tmp2.createNewFile()

            println "Processing $dirName"
            tmp2.withWriter { out ->
                fileNames.each { fileName ->
                    // println "Processing $dirName/$fileName"
                    def md5 = generateMD5(new File("$data/$dirName/$fileName"))
                    out.println("$md5 $fileName")
                }
                println "Done : " + dirName
            }
        }
    }
}
