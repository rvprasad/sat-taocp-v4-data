This repository provides a sketch of how [SAT Examples](http://www-cs-faculty.stanford.edu/~knuth/programs/SATexamples.tgz) from [*Donald Knuth's The Art of Computer Programming Volume 4 Fascicle 6 Satisfiability*](http://www.informit.com/store/art-of-computer-programming-volume-4-fascicle-6-satisfiability-9780134397603) are laid out in our experiments.  Instead of placing large amounts of data in the repository, we place the checksum of formulae (files) in the repository.  This serves two purposes:

 - It provides definitive information about the folder structure and the location of formulae within this structure as used in our experiments.
 - It enables parity check between the formulae used in our experiments and the original data set (or subsequent replication efforts).

## Attribution

Copyright (c) 2017, Venkatesh-Prasad Ranganath

Licensed under BSD 3-clause "New" or "Revised" License (https://choosealicense.com/licenses/bsd-3-clause/)

**Authors:** Venkatesh-Prasad Ranganath
